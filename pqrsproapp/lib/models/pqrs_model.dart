class Pqrs {
  int id;
  int idestudiante;
  String nombreestudiante;
  String area;
  String descripcion;
  String estado;
  String correo;


  Pqrs({id ,idestudiante  , nombreestudiante, area, descripcion, estado, correo});

  factory Pqrs.fromJson(Map<String, dynamic> ParsedJson) {
    return Pqrs(
      id: ParsedJson["id"],
      idestudiante: ParsedJson["idestudiante"],
      nombreestudiante: ParsedJson["nombreestudiante"],
      area: ParsedJson["area"],
      descripcion: ParsedJson["descripcion"],
      estado: ParsedJson["estado"],
      correo: ParsedJson["correo"],
    );
  }

  Map<String, dynamic> ToJson() => {
        "id": id,
        "idestudiante": idestudiante,
        "nombreestudiante": nombreestudiante,
        "area": area,
        "descripcion": descripcion,
        "estado": estado,
        "correo": correo
      };

  @override
  String toString() {
    return 'Pqrs{id: $id, idestudiante: $idestudiante, nombreestudiante: $nombreestudiante, area: $area, descripcion: $descripcion, estado: $estado, correo: $correo}';
  }
}
