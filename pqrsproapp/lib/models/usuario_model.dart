class Usuario {
  int id;
  String nombre;
  String apellido;
  String correo;
  String username;
  String password;
  int rol;

  Usuario({id, nombre, apellido, correo, username, password, rol});

  factory Usuario.fromJson(Map<String, dynamic> ParsedJson) {
    return Usuario(
      id: ParsedJson["id"],
      nombre: ParsedJson["nombre"],
      apellido: ParsedJson["apellido"],
      correo: ParsedJson["correo"],
      username: ParsedJson["username"],
      password: ParsedJson["password"],
      rol: ParsedJson["rol"],
    );
  }

  Map<String, dynamic> ToJson() => {
        "id": id,
        "nombre": nombre,
        "apellido": apellido,
        "correo": correo,
        "username": username,
        "password": password,
        "rol": rol
      };

  @override
  String toString() {
    return 'Usuario{id: $id, nombre: $nombre, apellido: $apellido, correo: $correo, username: $username, password: $password, rol: $rol}';
  }
}
