class ErrorApiResponse {
  String error;
  String message;

  ErrorApiResponse({error, message});

  factory ErrorApiResponse.fromJson(Map<String, dynamic> ParsedJson) {
    return ErrorApiResponse(
      error: ParsedJson["error"],
      message: ParsedJson["message"],
    );
  }

  Map<String, dynamic> ToJson() => {
    "error": error,
    "message": message,
  };

}
