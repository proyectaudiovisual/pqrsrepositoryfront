
import 'package:pqrsproapp/models/apiresponse_model.dart';
import 'package:pqrsproapp/models/pqrs_model.dart';
import 'package:pqrsproapp/models/usuario_model.dart';
import 'package:pqrsproapp/repository/usuario_Api_Service.dart';
import 'Pqrs_Api_Service.dart';

class Repository {
  Repository();

  UserApiServices userApiServices = UserApiServices();
  PqrsApiServices pqrApiServices = PqrsApiServices();

  Future<ApiResponse> login(String user, String pass) =>
      userApiServices.ValidarUsuario(user, pass);
  Future<ApiResponse> register(Usuario user) =>
      userApiServices.RegistrarUsiario(user);
  Future<ApiResponse> restablecer(Usuario user) =>
      userApiServices.RestablecerContrsena(user);
  Future<ApiResponse> registerPqrs(Pqrs Pqrs ,String token) =>
      pqrApiServices.RegistrarPqrs(Pqrs, token);
}
