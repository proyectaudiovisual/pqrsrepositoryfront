import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:pqrsproapp/Constants/constants.dart';
import 'package:pqrsproapp/models/apiresponse_model.dart';
import 'package:pqrsproapp/models/error_model.dart';
import 'package:pqrsproapp/models/token_model.dart';
import 'package:pqrsproapp/models/usuario_model.dart';
import 'package:http/http.dart' as http;

class UserApiServices {
  Usuario _usuario;
  TokenpiResponse _token;
  ErrorApiResponse _error;
  final _constants = Constants();

  UserApiServices();

  Future<ApiResponse> ValidarUsuario(String user, String pass) async {
    Map data = {'username': user, 'password': pass};
    ApiResponse apiResponse = ApiResponse();
    var body = json.encode(data);
    var response = await http
        .post(
            'http://ec2-52-14-8-153.us-east-2.compute.amazonaws.com:9091/user/logear',
            headers: {'Content-Type': 'application/json'},
            body: body)
        .then((response) {
      var resBody = json.decode(response.body)['payload'];
      apiResponse.statusResponse = response.statusCode;
      if (apiResponse.statusResponse == 200 && resBody != "error") {
        apiResponse.object = resBody;
        apiResponse.message = 'succes!';
        print(apiResponse.object);
      } else {
        _error = ErrorApiResponse.fromJson(resBody);
        apiResponse.object = _error;
        apiResponse.message = 'failure!';
      }
      return apiResponse;
    });
    return response;
  }

  //REGISTO
  Future<ApiResponse> RegistrarUsiario(Usuario user) async {
    Map data = user.ToJson();
    var body = json.encode(data);
    ApiResponse apiResponse = ApiResponse();
    var response = await http
        .post(
            'http://ec2-52-14-8-153.us-east-2.compute.amazonaws.com:9091/user/registrar',
            headers: {'Content-Type': 'application/json'},
            body: body)
        .then((response) {
      var resBody = json.decode(response.body)['payload'];
      apiResponse.statusResponse = response.statusCode;
      if (apiResponse.statusResponse == 200) {
        apiResponse.object = resBody;
        apiResponse.message = 'succes!';
        print(apiResponse.object);
      } else {
        _error = ErrorApiResponse.fromJson(resBody);
        apiResponse.object = _error;
        apiResponse.message = 'failure!';
      }
      return apiResponse;
    });
    return response;
  }

  Future<ApiResponse> RestablecerContrsena(Usuario user) async {
    Map data = user.ToJson();
    ApiResponse apiResponse = ApiResponse();
    var body = json.encode(data);
    var response = await http
        .post(
            'http://ec2-52-14-8-153.us-east-2.compute.amazonaws.com:9091/user/cambiarclave',
            headers: {'Content-Type': 'application/json'},
            body: body)
        .then((response) {
      var status = json.decode(response.body)['status'];
      apiResponse.statusResponse = status;
      if (apiResponse.statusResponse == 200) {
        _usuario = Usuario.fromJson(json.decode(response.body)['payload']);
        apiResponse.object = _usuario;
        apiResponse.message = 'succes!';
      } else {
        apiResponse.object = json.decode(response.body)['payload'];
        apiResponse.message = 'failure!';
      }
      return apiResponse;
    });
    print(apiResponse.object);
    return response;
  }

  Future<bool> isSignedIn() async {
    return true;
  }
}
