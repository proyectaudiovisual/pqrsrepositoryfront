import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pqrsproapp/Screen/registerpqrs.dart';
import 'package:pqrsproapp/models/token_model.dart';

class Home extends StatefulWidget {
  // ignore: non_constant_identifier_names
  Home({Key key, this.title, this.token}) : super(key: key);
  final String title;
  final String token;

  @override
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<Home> {
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Bienvenido '),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.add),
            tooltip: 'Crear PQRS',
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (BuildContext context) => RegisterPqrs(
                    token: this.widget.token,
                  ),
                ),
              );
            },
          ),
        ],
      ),
      backgroundColor: Colors.white,
    );
  }
}
