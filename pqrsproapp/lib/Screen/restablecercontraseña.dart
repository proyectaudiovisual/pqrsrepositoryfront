import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pqrsproapp/Bloc/user_bloc.dart';
import 'package:pqrsproapp/models/usuario_model.dart';

import 'home.dart';
import 'login.dart';

class RestablecerContrasenaUser extends StatefulWidget {
  RestablecerContrasenaUser({Key key, this.title, this.token}) : super(key: key);

  final String title;
  final String token;

  @override
  MyRestablecerUserState createState() => MyRestablecerUserState();
}

class MyRestablecerUserState extends State<RestablecerContrasenaUser> {
  UserBloc _userBloc;
  final _formKey = GlobalKey<FormState>();
  Usuario user;

  @override
  void initState() {
    super.initState();
    user = new Usuario();
    _userBloc = UserBloc(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Restablecer contraseña'),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: [
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Image.asset(
                    'assets/img/pqrs.png',
                    height: 180,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.person), onPressed: null),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: TextFormField(
                              decoration: InputDecoration(hintText: 'Usuario'),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Ingrese el Usuarion';
                                }
                                user.username = value;
                                return null;
                              },
                              onSaved: (String value) {
                                if (value.isNotEmpty) {
                                  user.username = value;
                                }
                              }),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.mail), onPressed: null),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: TextFormField(
                              decoration: InputDecoration(hintText: 'Email'),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Ingrese el email';
                                }
                                user.correo = value;
                                return null;
                              },
                              onSaved: (String value) {
                                if (value.isNotEmpty) {
                                  user.correo = value;
                                }
                              }),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Container(
                      child: RaisedButton(
                        onPressed: _submitRestablecer,
                        child: Text("Restablecer"),
                        color: Colors.teal,
                        clipBehavior: Clip.hardEdge,
                        elevation: 10,
                        disabledColor: Colors.blueGrey,
                        disabledElevation: 10,
                        disabledTextColor: Colors.white,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error al restablecer la contraseña'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Error ingrese todos los datos'),
                Text('Vuelva a ingresar los datos'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Approve'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }



  Future<void> _submitRestablecer() async {
    final FormState formState = _formKey.currentState;
    if (!formState.validate()) {
      _showMyDialog();
    } else {
      user.rol=0;
      user.password = "null";
      user.nombre = "null";
      user.apellido = "null";
      user.id = 0;
      formState.save();
      print(user.toString());
      _userBloc.restablecer(user).then((value) {
        print(value);
        if (value != 200) {
          _showMyDialog();
        } else {
          print("Exito");
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => Login(),
            ),
          );
        }
      });
    }
  }
}
