import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pqrsproapp/Bloc/user_bloc.dart';
import 'package:pqrsproapp/models/usuario_model.dart';

import 'home.dart';
import 'login.dart';

class RegisterUser extends StatefulWidget {
  RegisterUser({Key key, this.title, this.token}) : super(key: key);

  final String title;
  final String token;

  @override
  _MyRegisterUserState createState() => _MyRegisterUserState();
}

class _MyRegisterUserState extends State<RegisterUser> {
  UserBloc _userBloc;
  final _formKey = GlobalKey<FormState>();
  Usuario user;

  @override
  void initState() {
    super.initState();
    user = new Usuario();
    _userBloc = UserBloc(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Crear Nuevo Usuario'),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: [
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.person), onPressed: null),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: TextFormField(
                              decoration: InputDecoration(hintText: 'Nombre'),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Ingrese el nombre';
                                }
                                user.nombre = value;
                                return null;
                              },
                              onSaved: (String value) {
                                if (value.isNotEmpty) {
                                  user.nombre = value;
                                }
                              }),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.person), onPressed: null),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: TextFormField(
                              decoration: InputDecoration(hintText: 'Apellido'),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Ingrese el apellido';
                                }
                                user.apellido = value;
                                return null;
                              },
                              onSaved: (String value) {
                                if (value.isNotEmpty) {
                                  user.apellido = value;
                                }
                              }),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.person), onPressed: null),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: TextFormField(
                              decoration: InputDecoration(hintText: 'Usuario'),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Ingrese el Usuarion';
                                }
                                user.username = value;
                                return null;
                              },
                              onSaved: (String value) {
                                if (value.isNotEmpty) {
                                  user.username = value;
                                }
                              }),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.lock), onPressed: null),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: TextFormField(
                              decoration: InputDecoration(hintText: 'Password'),
                              obscureText: true,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Ingrese la contraseña';
                                }
                                user.password = value;
                                return null;
                              },
                              onSaved: (String value) {
                                if (value.isNotEmpty) {
                                  user.password = value;
                                }
                              }),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.mail), onPressed: null),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: TextFormField(
                              decoration: InputDecoration(hintText: 'Email'),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Ingrese el email';
                                }
                                user.correo = value;
                                return null;
                              },
                              onSaved: (String value) {
                                if (value.isNotEmpty) {
                                  user.correo = value;
                                }
                              }),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Container(
                      child: RaisedButton(
                        onPressed: _submitRegister,
                        child: const Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                        color: Colors.teal,
                        clipBehavior: Clip.hardEdge,
                        elevation: 10,
                        disabledColor: Colors.blueGrey,
                        disabledElevation: 10,
                        disabledTextColor: Colors.white,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error al registrar'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Error ingrese todos los datos'),
                Text('Vuelva a ingresar los datos'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Approve'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _submitRegister() async {
    final FormState formState = _formKey.currentState;
    if (!formState.validate()) {
      _showMyDialog();
    } else {
      user.id = 1;
      user.rol = 1;
      formState.save();
      print(user.toString());
      _userBloc.Register(user).then((value) {
        print(value);
        if (value != 200) {
          _showMyDialog();
        } else {
          print("Exito");
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => Login(),
            ),
          );
        }
      });
    }
  }
}
