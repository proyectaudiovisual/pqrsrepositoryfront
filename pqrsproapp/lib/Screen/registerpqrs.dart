import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pqrsproapp/Bloc/pqrs_bloc.dart';
import 'package:pqrsproapp/models/pqrs_model.dart';
import 'package:pqrsproapp/models/token_model.dart';
import 'home.dart';

class RegisterPqrs extends StatefulWidget {
  RegisterPqrs({Key key, this.title, this.token}) : super(key: key);

  final String title;
  final String token;

  @override
  _MyRegisterPqrsState createState() => _MyRegisterPqrsState();
}

class _MyRegisterPqrsState extends State<RegisterPqrs> {
  PqrsBloc _pqrsBloc;
  final _formKey = GlobalKey<FormState>();
  Pqrs pqrs = new Pqrs(
      id: 32,
      idestudiante: 1,
      nombreestudiante: "Felipe",
      area: "UNIJAC",
      descripcion: "Prueba",
      estado: "A",
      correo: "Rincon3549");

  @override
  void initState() {
    super.initState();
    _pqrsBloc = PqrsBloc(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Crear Nueva PQRS'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.add),
            tooltip: 'Show Snackbar',
            onPressed: () {},
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: [
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.person), onPressed: null),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: TextFormField(
                            decoration: InputDecoration(hintText: 'Nombre'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Ingrese su nombre';
                              }
                              pqrs.nombreestudiante = value;
                              return null;
                            },
                            onSaved: (String value) {
                              if (value.isNotEmpty) {
                                pqrs.nombreestudiante = value;
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.api), onPressed: null),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: TextFormField(
                            decoration: InputDecoration(hintText: 'Area'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Ingrese el area';
                              }
                              pqrs.nombreestudiante = value;
                              return null;
                            },
                            onSaved: (String value) {
                              if (value.isNotEmpty) {
                                pqrs.area = value;
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.description), onPressed: null),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: TextFormField(
                            decoration:
                                InputDecoration(hintText: 'Descripción'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Ingrese la descripción';
                              }
                              pqrs.nombreestudiante = value;
                              return null;
                            },
                            onSaved: (String value) {
                              if (value.isNotEmpty) {
                                pqrs.descripcion = value;
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.mail), onPressed: null),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: TextFormField(
                            decoration: InputDecoration(hintText: 'Email'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Ingrese el email';
                              }
                              pqrs.correo = value;
                              return null;
                            },
                            onSaved: (String value) {
                              if (value.isNotEmpty) {
                                pqrs.correo = value;
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Container(
                      child: RaisedButton(
                        onPressed: _submitRegister,
                        child: const Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                        color: Colors.teal,
                        clipBehavior: Clip.hardEdge,
                        elevation: 10,
                        disabledColor: Colors.blueGrey,
                        disabledElevation: 10,
                        disabledTextColor: Colors.white,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error al registrar'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Error ingrese todos los datos'),
                Text('Vuelva a ingresar los datos'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Approve'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _submitRegister() async {
    final FormState formState = _formKey.currentState;
    if (!formState.validate()) {
      _showMyDialog();
    } else {
      pqrs.id = 32;
      pqrs.idestudiante = 0;
      pqrs.estado = "A";
      formState.save();
      _pqrsBloc.registerPqrs(pqrs, this.widget.token).then((value) {
        if (value.statusResponse != 200) {
          _showMyDialog();
        } else {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => Home(
                token: this.widget.token,
              ),
            ),
          );
        }
      });
    }
  }
}
