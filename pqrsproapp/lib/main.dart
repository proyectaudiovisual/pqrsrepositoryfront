import 'package:flutter/material.dart';
import 'package:pqrsproapp/Screen/login.dart';


void main() {
  runApp(PqrsApp());
}

class PqrsApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PQRS',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home:Login(title: 'application pqrs'),
    );
  }
}

