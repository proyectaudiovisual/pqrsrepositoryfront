import 'package:flutter/cupertino.dart';
import 'package:pqrsproapp/Constants/constants.dart';
import 'package:pqrsproapp/Constants/constantsError.dart';
import 'package:pqrsproapp/models/apiresponse_model.dart';
import 'package:pqrsproapp/models/pqrs_model.dart';
import 'package:pqrsproapp/models/token_model.dart';
import 'package:pqrsproapp/repository/generalpqrs_repository.dart';

class PqrsBloc {
  BuildContext _context;
  Repository _repository = Repository();
  final constants = Constants();
  final constantsError = ConstantsError();

  PqrsBloc(BuildContext context) {
    _context = context;
  }

  // ignore: missing_return
  Future<ApiResponse> registerPqrs(Pqrs pqrs, String token) async {
    var respuesta = await _repository.registerPqrs(pqrs, token);
    return respuesta;
  }

  @override
  Stream<LoginBlocState> mapEventToState(LoginBlocEvent event) {
    throw UnimplementedError();
  }
}

class LoginBlocState {}

class LoginBlocEvent {}
