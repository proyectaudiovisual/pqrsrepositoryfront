import 'package:flutter/cupertino.dart';
import 'package:pqrsproapp/Constants/constants.dart';
import 'package:pqrsproapp/Constants/constantsError.dart';
import 'package:pqrsproapp/models/apiresponse_model.dart';
import 'package:pqrsproapp/models/usuario_model.dart';
import 'package:pqrsproapp/repository/generalpqrs_repository.dart';

class UserBloc {
  BuildContext _context;
  final _repository = Repository();
  final constants = Constants();
  final constantsError = ConstantsError();

  UserBloc(BuildContext context){
    _context = context;
  }

  // ignore: missing_return
  Future<ApiResponse> loginUser(String user, String password) async {
    ApiResponse response = await _repository.login(user, password);
    return response;
  }

  Future<ApiResponse> Register(Usuario registro) async {
    var response = await _repository.register(registro);
    return response;
  }

  Future<ApiResponse> restablecer(Usuario registroclave) async {
    var response = await _repository.restablecer(registroclave);
    return response;
  }

  @override
  Stream<LoginBlocState> mapEventToState(LoginBlocEvent event) {
    throw UnimplementedError();
  }
}

class LoginBlocState {}

class LoginBlocEvent {}
